<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>人格整形所</title>

</head>

<body background="https://publicdomainq.net/images/201708/07s/publicdomainq-0011978gsy.jpg">
<div align = "center">

<p style = "font-size:800%">
<font color = "white">
    💀ユーザー情報更新画面💀
</font>
</p>

<!-- エラーフラグが立ったら表示 -->
	<c:if test = "${era1 != null}" var ="flg" />
	<c:if test="${flg}">
	<font color="red">！！！※入力された内容は正しくありません※！！！<br>パスワードの違い、未入力欄があるかもだから直して！！！</font>
	<%request.removeAttribute("era"); %>
	</c:if>

	<!-- エラーフラグが立ったら表示 -->
	<c:if test = "${era2 != null}" var ="flg" />
	<c:if test="${flg}">
	<font color="red">！！！※致命的エラー※！！！<br>やヴぁいですわね</font>
	<%request.removeAttribute("era2"); %>
	</c:if>

<form action="/UserManagement/User_Update" method="post">
<p style = "font-size:200%">
<font color="white">


ログインID : ${sanid}




<br><br>

パスワード : <input type="text" name="pass" size="20"placeholder="パスワード入力">

<br><br>

パスワード(確認) : <input type="text" name="passkaku" size="20"placeholder="パスワード再入力">　　　

<br><br>


<c:if test = "${sanname != null}" var ="flg" />
	<c:if test="${flg}">
ユーザー名 : <input type="text" name="sanname" size="20"value="${sanname}"placeholder="名前入力"required>
	</c:if>
<c:if test="${!flg}">
ユーザー名 : <input type="text" name="sanname" size="20"placeholder="名前入力"required>
	</c:if>



<br><br>

<c:if test = "${santan != null}" var ="flg" />
	<c:if test="${flg}">
生年月日 : <input type="date" name="santan" value="${santan}"required>
	<%request.removeAttribute("seneh"); %>
	</c:if>
<c:if test="${!flg}">
生年月日 : <input type="date" name="santan" value="2000-01-01"required>
	<%request.removeAttribute("seneh"); %>
	</c:if>


<input type="hidden" name="sanid" value="${sanid}">

<br><input type="submit" value="ユーザー情報更新！"required>

</font>

</p>
</form>

<form name="formname" method="POST" action="/">
<a href="User_List">戻る</a>
</form>



</div>
</body>
</html>