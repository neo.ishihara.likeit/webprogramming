

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AllUserList;
import mame.User;

/**
 * Servlet implementation class User_Detail
 */
public class User_Detail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public User_Detail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession();
		request.setCharacterEncoding("utf-8");
		if(session.getAttribute("idname") != null)
		{
			RequestDispatcher dis = request.getRequestDispatcher("WEB-INF/jsp/userDetail.jsp");
			dis.forward(request, response);
			return ;
		}

		RequestDispatcher di = request.getRequestDispatcher("WEB-INF/jsp/userLogin.jsp");
		di.forward(request, response);
		return ;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//HttpSession session = request.getSession();
		request.setCharacterEncoding("utf-8");

		ArrayList<User> list =AllUserList.findAll(request.getParameter("sanid"), null, null, null);

		for(User u:list)
		{
			request.setAttribute("rogu", u.getLogin_id());
			request.setAttribute("name", u.getName());
			request.setAttribute("tan", u.getBarth_date());
			request.setAttribute("sin", u.getCreate_date());
			request.setAttribute("kou", u.getUpdate_date());
		}

		doGet(request, response);
	}

}
