package mame;

import java.sql.Date;
import java.sql.Timestamp;

public class User {
	private String id;
	private String login_id;
	private String name;
	private Date barth_date;
	private String password;
	private Timestamp create_date;
	private Timestamp update_date;

	public User() {

	}

	public User(String id,String login_id,String name,Date barth_date, String password, Timestamp create_date, Timestamp update_date)
	{
		this.id = id;
		this.login_id = login_id;
		this.name = name;
		this.barth_date = barth_date;
		this.password = password;
		this.create_date = create_date;
		this.update_date = update_date;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBarth_date() {
		return barth_date;
	}

	public void setBarth_date(Date barth_date) {
		this.barth_date = barth_date;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Timestamp getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Timestamp create_date) {
		this.create_date = create_date;
	}

	public Timestamp getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Timestamp update_date) {
		this.update_date = update_date;
	}
}
