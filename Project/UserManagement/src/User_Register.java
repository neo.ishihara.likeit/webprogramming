

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class User_Register
 */
public class User_Register extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public User_Register() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//セッションがなかった場合ログイン画面に遷移
		HttpSession session = request.getSession();
		if(session.getAttribute("idint") == null){
			RequestDispatcher dis =request.getRequestDispatcher("WEB-INF/jsp/userLogin.jsp");
			dis.forward(request, response);
			return ;
		}
		if(session.getAttribute("idint").equals("1"))
		{
			RequestDispatcher dis = request.getRequestDispatcher("WEB-INF/jsp/userRegister.jsp");
			dis.forward(request, response);
			return ;
		}else {
			request.setAttribute("noadmin", "1");
			RequestDispatcher di = request.getRequestDispatcher("User_List");
			di.forward(request, response);
			return ;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");

		// TODO Auto-generated method stub
		//管理者以外はじく
		HttpSession session = request.getSession();

		if(session.getAttribute("idint") == null){
			RequestDispatcher dis =request.getRequestDispatcher("WEB-INF/jsp/userLogin.jsp");
			dis.forward(request, response);
			return ;
		}
		if(!session.getAttribute("idint").equals("1"))
		{
			request.setAttribute("noadmin", "1");
			RequestDispatcher di = request.getRequestDispatcher("User_List");
			di.forward(request, response);
			return ;
		}

		@SuppressWarnings("unused")
		boolean error = false;
		String roginid = request.getParameter("roginid");
		String pass = request.getParameter("pass");
		String passkaku = request.getParameter("passkaku");
		String name = request.getParameter("name");

		Timestamp times = new Timestamp(System.currentTimeMillis());
		Timestamp createdate = times;

		String date = request.getParameter("birthdate");
		Date datedate = Date.valueOf(date);


		//未入力発見器
		if(roginid == null || pass ==null || passkaku ==null || name ==null || date ==null || pass.equals(""))
			error=true;
		 if(!pass.equals(passkaku))
			error=true;
		 if(dao.AllUserList.loginidcheck(roginid))
			error=true;

		//早期発見次第エラー発信
		if(error == true) {
			request.setAttribute("era1", "1");
			request.setAttribute("roguh", roginid);
			request.setAttribute("nameh", name);
			request.setAttribute("seneh", datedate);
			RequestDispatcher di = request.getRequestDispatcher("WEB-INF/jsp/userRegister.jsp");
			di.forward(request, response);
			return ;
		}

		//追加すっぞ！
		try {
			error = dao.AllUserList.Register(roginid, name, datedate, pass, createdate, createdate);
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		//最初の関門を突破してここまできて登録失敗とな...?
		if(error == true) {
			request.setAttribute("era2", "1");
			RequestDispatcher di = request.getRequestDispatcher("WEB-INF/jsp/userRegister.jsp");
			di.forward(request, response);
			return;
		}
		//成功次第リストｊｓｐへ遷移
		RequestDispatcher di = request.getRequestDispatcher("User_List");
		di.forward(request, response);
		return;
	}

}
