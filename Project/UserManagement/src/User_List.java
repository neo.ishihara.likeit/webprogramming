import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AllUserList;
import mame.User;

/**
 * Servlet implementation class User_List
 */
public class User_List extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public User_List() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
		return ;

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		request.setCharacterEncoding("utf-8");


		//id passwordがスコープにあればログイン処理
				if (request.getParameter("loginid") != null && request.getParameter("password") != null)
				{
					try {
						String inputid = request.getParameter("loginid");
						String inputpass = dao.Angou.angouka(request.getParameter("password"));
						String idname = AllUserList.loginname(inputid, inputpass);
						String idint = AllUserList.loginid(inputid, inputpass);
						//成功したらセッションにIDとNAMEを登録
						if(idname != null && idint != null) {
							session.setAttribute("idint", idint);
							session.setAttribute("idname", idname);
							session.setAttribute("idroguin", inputid);
						}else {
							request.setAttribute("tigau", "1");
							RequestDispatcher di = request.getRequestDispatcher("WEB-INF/jsp/userLogin.jsp");
							di.forward(request, response);
							return ;
						}
					} catch (NoSuchAlgorithmException e) {
						// TODO 自動生成された catch ブロック
						e.printStackTrace();
					}
				}


		//リスト表示処理
		ArrayList<User> list = new ArrayList<User>();
		int ultrasoul = 0;
		String date1 = null;
		String date2 = null;
		//検索分岐
		if (request.getParameter("roginid") != null || request.getParameter("name") != null||
			(request.getParameter("date1") != null && request.getParameter("date2") != null))
		{
			if(request.getParameter("roginid") != "")
				ultrasoul += 1;
			if(request.getParameter("name") != "")
				ultrasoul += 2;
			if(request.getParameter("date1") != "" && request.getParameter("date2") != "") {
				date1 = request.getParameter("date1");
				date2 = request.getParameter("date2");
				ultrasoul += 4;
			}
		}
		switch (ultrasoul) {
		case 0:
			list = AllUserList.findAll(null, null, null, null);
			break;
		case 1:
			list = AllUserList.findAll(request.getParameter("roginid"), null, null, null);
			break;
		case 2:
			list = AllUserList.findAll(null, request.getParameter("name"), null, null);
			break;
		case 3:
			list = AllUserList.findAll(request.getParameter("roginid"), request.getParameter("name"), null, null);
			break;
		case 4:
			list = AllUserList.findAll(null, null, date1, date2);
			break;
		case 5:
			list = AllUserList.findAll(request.getParameter("roginid"), null, date1, date2);
			break;
		case 6:
			list = AllUserList.findAll(null, request.getParameter("name"), date1, date2);
			break;
		case 7:
			list = AllUserList.findAll(request.getParameter("roginid"), request.getParameter("name"), date1, date2);
			break;
		}


		StringBuilder table = new StringBuilder();

		for(User ulist : list)
		{
			table.append("<tr>");
			//loginid
			table.append("<td>");
			table.append(ulist.getLogin_id());
			table.append("</td>");
			//name
			table.append("<td>");
			table.append(ulist.getName());
			table.append("</td>");
			//otanjoubi
			table.append("<td>");
			table.append(ulist.getBarth_date());
			table.append("</td>");

			table.append("<td><div style=\"display:inline-flex\">");

			table.append("<form action=\"/UserManagement/User_Detail\" method=\"post\">");
			table.append("<input type=\"hidden\" name=\"sanid\"value=\""+ ulist.getLogin_id() +"\">");
			table.append("<input type=\"submit\" value=\"参照\"class=\"btn1-square\">");
			table.append("</form>");

			if (ulist.getLogin_id().equals(session.getAttribute("idroguin")) || session.getAttribute("idint").equals("1"))
			{
				table.append("<form action=\"/UserManagement/User_Update\" method=\"post\">");
				table.append("<input type=\"hidden\" name=\"sanid\"value=\""+ ulist.getLogin_id() +"\">");
				table.append("<input type=\"hidden\" name=\"sanname\"value=\""+ ulist.getName() +"\">");
				table.append("<input type=\"hidden\" name=\"santan\"value=\""+ ulist.getBarth_date() +"\">");
				table.append("<input type=\"submit\" value=\"更新\"class=\"btn2-square\">");
				table.append("</form>");
			}



			if (session.getAttribute("idint").equals("1"))
			{
				table.append("<form action=\"/UserManagement/User_Delete\" method=\"post\">");
				table.append("<input type=\"hidden\" name=\"sanid\"value=\""+ ulist.getLogin_id() +"\">");
				table.append("<input type=\"submit\" value=\"削除\" class=\"btn3-square\">");
				table.append("</form>");
			}


			table.append("</div></td>");

			table.append("</tr>");
		}

		request.setAttribute("table", table.toString());

		//セッションがなかった場合ログイン画面に遷移
		if(session.getAttribute("idname") != null)
		{
			RequestDispatcher dis = request.getRequestDispatcher("WEB-INF/jsp/userList.jsp");
			dis.forward(request, response);
			return ;
		}

		RequestDispatcher di = request.getRequestDispatcher("WEB-INF/jsp/userLogin.jsp");
		di.forward(request, response);
		return ;
	}

}
