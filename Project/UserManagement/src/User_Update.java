

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class User_Update
 */
public class User_Update extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public User_Update() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher di = request.getRequestDispatcher("WEB-INF/jsp/userList.jsp");
		di.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		//HttpSession session = request.getSession();


		boolean error = false;
		String loginid = request.getParameter("sanid");
		String pass = request.getParameter("pass");
		String passkaku = request.getParameter("passkaku");
		String name = request.getParameter("sanname");

		request.removeAttribute("sanid");
		request.setAttribute("sanid", loginid);
		request.removeAttribute("sanname");
		request.setAttribute("sanname", name);
		request.setAttribute("santan", request.getParameter("santan"));

		Timestamp times = new Timestamp(System.currentTimeMillis());
		Timestamp update = times;

		String date = request.getParameter("santan");


		if(pass != null && passkaku != null)
			if(!pass.equals(passkaku))
				error=true;

		//早期発見次第エラー発信
		if(error == true) {
			request.setAttribute("era1", "1");
			request.setAttribute("sanname", name);
			request.setAttribute("santan", date);
			RequestDispatcher di = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
			di.forward(request, response);
			return ;
		}

		//更新すっぞ！
		if(pass != null && passkaku != null)
		{
			try {
				if(pass != "")
					error = dao.AllUserList.Update(loginid, name, date, pass, update);
				else
					error = dao.AllUserList.Update(loginid, name, date, null, update);
				if(error == false)
				{
					//成功次第リストｊｓｐへ遷移
					RequestDispatcher di = request.getRequestDispatcher("User_List");
					di.forward(request, response);
					return;
				}
			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
		}
		//最初の関門を突破してここまできて登録失敗とな...?
		if(error == true) {
			request.setAttribute("era2", "1");
			RequestDispatcher di = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
			di.forward(request, response);
			return;
		}
		RequestDispatcher di = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
		di.forward(request, response);
		return;
	}
}