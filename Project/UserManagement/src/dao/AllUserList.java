package dao;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import mame.User;
public class AllUserList {
	//List検索
	public static ArrayList<User> findAll(String loginid,String name,String bdate1,String bdate2){
		Connection con = null;
		PreparedStatement stmt = null;
		ArrayList<User> list = new ArrayList<User>();
		try {

			con = DBManager.getConnection();
			String sql = null;

			int ultrasoul = 0;
					if(loginid != null)
						ultrasoul += 1;
					if(name != null)
						ultrasoul += 2;
					if(bdate1 != null && bdate2 != null) {
						ultrasoul += 4;
					}
				switch (ultrasoul) {
				case 0:
					sql = "SELECT * FROM user WHERE id NOT IN ('1');";
					stmt = con.prepareStatement(sql);
					break;
				case 1:
					sql = "SELECT * FROM user WHERE id NOT IN ('1') AND login_id =  ? ;";
					stmt = con.prepareStatement(sql);
					stmt.setString(1, loginid);
					break;
				case 2:
					sql = "SELECT * FROM user WHERE id NOT IN ('1') AND name LIKE ?;";
					stmt = con.prepareStatement(sql);
					stmt.setString(1, "%"+name+"%");
					break;
				case 3:
					sql = "SELECT * FROM user WHERE id NOT IN ('1') AND login_id =  ? AND name LIKE ? ;";
					stmt = con.prepareStatement(sql);
					stmt.setString(1, loginid);
					stmt.setString(2, "%"+name+"%");
					break;
				case 4:
					sql = "SELECT * FROM user WHERE id NOT IN ('1') AND birth_date >= ? AND birth_date <= ? ;";
					stmt = con.prepareStatement(sql);
					stmt.setString(1, bdate1);
					stmt.setString(2, bdate2);
					break;
				case 5:
					sql = "SELECT * FROM user WHERE id NOT IN ('1') AND login_id =  ? AND birth_date >= ? AND birth_date <= ? ;";
					stmt = con.prepareStatement(sql);
					stmt.setString(1, loginid);
					stmt.setString(2, bdate1);
					stmt.setString(3, bdate2);
					break;
				case 6:
					sql = "SELECT * FROM user WHERE id NOT IN ('1') AND name LIKE ? AND birth_date >= ? AND birth_date <= ?;";
					stmt = con.prepareStatement(sql);
					stmt.setString(1, "%"+name+"%");
					stmt.setString(2, bdate1);
					stmt.setString(3, bdate2);
					break;
				case 7:
					sql = "SELECT * FROM user WHERE id NOT IN ('1') AND login_id = ? AND name LIKE ? AND birth_date >= ? AND birth_date <= ?;";
					stmt = con.prepareStatement(sql);
					stmt.setString(1, loginid);
					stmt.setString(2, "%"+name+"%");
					stmt.setString(3, bdate1);
					stmt.setString(4, bdate2);
					break;
				}


			ResultSet rs = stmt.executeQuery();

			while(rs.next())
			{
				User a = new User();
				a.setId(rs.getString("id"));
				a.setLogin_id(rs.getString("login_id"));
				a.setName(rs.getString("name"));
				a.setBarth_date(rs.getDate("birth_date"));
				a.setPassword(rs.getString("password"));
				a.setCreate_date(rs.getTimestamp("create_date"));
				a.setUpdate_date(rs.getTimestamp("update_date"));
				list.add(a);
			}
		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(stmt != null)
					stmt.close();
				if(con != null)
					con.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	//ログイン検索　一致するものがあるかどうか 一致したらnameを返す
	public static String loginname(String inputid, String inputpass) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id=? AND password =?";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, inputid);
			stmt.setString(2, inputpass);
			ResultSet rs = stmt.executeQuery();
			if(!(rs.next()))
				return null;
			return rs.getString("name");
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(stmt != null)
					stmt.close();
				if(con != null)
					con.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	//検索しid番号を返す
	public static String loginid(String inputid, String inputpass) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id=? AND password =?";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, inputid);
			stmt.setString(2, inputpass);
			ResultSet rs = stmt.executeQuery();
			if(!(rs.next()))
				return null;
			return rs.getString("id");
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(stmt != null)
					stmt.close();
				if(con != null)
					con.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	//すでにあるログインIDかどうか あるtrue ないfalse
	public static boolean loginidcheck(String inputid) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id=?";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, inputid);
			ResultSet rs = stmt.executeQuery();
			if(rs.next())
				return true;
			return false;
		}catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if(stmt != null)
					stmt.close();
				if(con != null)
					con.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	//新規にUserを追加する
	public static boolean Register(String loginid, String name, Date birthdate, String password, Timestamp createdate, Timestamp updatedate) throws NoSuchAlgorithmException {
		Connection con =null;
		PreparedStatement stmt = null;

	try {
		con = DBManager.getConnection();
		String sql = "INSERT INTO user VALUES(null,?,?,?,?,?,?);";
		stmt = con.prepareStatement(sql);
		stmt.setString(1, loginid);
		stmt.setString(2, name);
		stmt.setDate(3, birthdate);
		stmt.setString(4, dao.Angou.angouka(password));
		stmt.setTimestamp(5, createdate);
		stmt.setTimestamp(6, updatedate);
		int rs = stmt.executeUpdate();

		if (rs > 0)
			return false;
		}catch (SQLException e) {
		e.printStackTrace();
	} finally {
		try {
			if(stmt != null)
				stmt.close();
			if(con != null)
				con.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	return true;
	}

	//キャラ整形
	public static boolean Update(String loginid, String name, String datedate, String pass, Timestamp update) throws NoSuchAlgorithmException {
		// TODO 自動生成されたメソッド・スタブ
		Connection con =null;
		PreparedStatement stmt = null;
		int rs = 0;
	try {
		con = DBManager.getConnection();
		if (pass != null) {
		String sql = "UPDATE user SET name = ? , birth_date = ? , password = ? , update_date = ? WHERE login_id = ? ;";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, name);
			stmt.setString(2, datedate);
			stmt.setString(3, dao.Angou.angouka(pass));
			stmt.setTimestamp(4, update);
			stmt.setString(5, loginid);
			rs = stmt.executeUpdate();
		}else {
			String sql = "UPDATE user SET name = ? , birth_date = ? , update_date = ? WHERE login_id = ? ;";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, name);
			stmt.setString(2, datedate);
			stmt.setTimestamp(3, update);
			stmt.setString(4, loginid);
			rs = stmt.executeUpdate();
		}
		}catch (SQLException e) {
		e.printStackTrace();
	} finally {
		try {
			if(stmt != null)
				stmt.close();
			if(con != null)
				con.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	if (rs > 0)
		return false;
	return true;
	}
	public static boolean udelete(String loginid) {
		// TODO 自動生成されたメソッド・スタブ
		Connection con =null;
		PreparedStatement stmt = null;
		int rs = 0;
	try {
		con = DBManager.getConnection();
		String sql = "DELETE FROM user WHERE login_id = ?;";
			stmt = con.prepareStatement(sql);
			stmt.setString(1, loginid);
			rs = stmt.executeUpdate();
		}catch (SQLException e) {
		e.printStackTrace();
	} finally {
		try {
			if(stmt != null)
				stmt.close();
			if(con != null)
				con.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	if (rs > 0)
		return false;
	return true;
	}
}