package dao;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

public class Angou {
	public static String angouka(String soosu) throws NoSuchAlgorithmException {
		String source = soosu;
		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";

		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		String result = DatatypeConverter.printHexBinary(bytes);
		return result;
	}
//	public static void main(String[] argc) throws NoSuchAlgorithmException {
//		String source = "password";
//		Charset charset = StandardCharsets.UTF_8;
//		String algorithm = "MD5";
//
//		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
//		String result = DatatypeConverter.printHexBinary(bytes);
//		System.out.println(result);
//	}
}
