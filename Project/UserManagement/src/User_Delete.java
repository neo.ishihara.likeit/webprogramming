

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.AllUserList;

/**
 * Servlet implementation class User_Delete
 */
public class User_Delete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public User_Delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("yes") != null) {
			String logid = (String) request.getParameter("sanid");
			AllUserList.udelete(logid);
			RequestDispatcher di = request.getRequestDispatcher("User_List");
			di.forward(request, response);
			return ;
		}
			String a = request.getParameter("sanid");
			request.setAttribute("sanid", a);
			RequestDispatcher di = request.getRequestDispatcher("WEB-INF/jsp/userDelete.jsp");
			di.forward(request, response);
			return ;
	}

}
